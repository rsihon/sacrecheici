from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin

from resa.views import index

admin.autodiscover()

urlpatterns = [
    path('', index),
    path('miango/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
