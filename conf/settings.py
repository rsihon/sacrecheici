import os
from django.utils.translation import ugettext_lazy as _
from decouple import config, Csv
from unipath import Path
from dj_database_url import parse as db_url

BASE_DIR = Path(__file__).parent.parent

SECRET_KEY = config('SECRET_KEY')

DEBUG = config('DEBUG', default=False, cast=bool)

ADMINS = config('ADMINS', default='', cast=lambda val: [('Admin', tk.strip()) for tk in val.split(',')])

TEMPLATE_DEBUG = config('TEMPLATE_DEBUG', default=False, cast=bool)

ALLOWED_HOSTS = config('ALLOWED_HOSTS', default='', cast=Csv())

IS_API = config('IS_API', default=False, cast=bool)

API_URL = config('API_URL', default='')


# Application definition


INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    "corsheaders.middleware.CorsMiddleware",
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django_user_agents.middleware.UserAgentMiddleware',
]

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'templates')
        ]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.template.context_processors.i18n',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

ROOT_URLCONF = 'conf.urls'

WSGI_APPLICATION = 'conf.wsgi.application'

DATABASES = {
    'default': config(
        'DATABASE_URL',
        default='sqlite:////' + BASE_DIR.child('db.sqlite3') + '?conn_max_age=0',
        cast=lambda val: db_url(val.split('?')[0], conn_max_age=int(val.split('?')[1].replace('conn_max_age=', '')))
    ),
    'umbrella': config(
        'UMBRELLA_DATABASE_URL',
        default='sqlite:////' + BASE_DIR.child('db.sqlite3') + '?conn_max_age=0',
        cast=lambda val: db_url(val.split('?')[0], conn_max_age=int(val.split('?')[1].replace('conn_max_age=', '')))
    ),
    'wallets': config(
        'WALLETS_DATABASE_URL',
        default='sqlite:////' + BASE_DIR.child('db.sqlite3') + '?conn_max_age=0',
        cast=lambda val: db_url(val.split('?')[0], conn_max_age=int(val.split('?')[1].replace('conn_max_age=', '')))
    ),
}

SESSION_COOKIE_NAME = config('SESSION_COOKIE_NAME', default='gw_sessionid')

LANGUAGE_CODE = config('LANGUAGE_CODE', default='en-us')

TIME_ZONE = config('TIME_ZONE', default='Africa/Douala')

USE_I18N = True

LANGUAGES = (
    ('en', _('English')),
    ('fr', _('French')),
)

LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'locale')
]

USE_L10N = True

USE_TZ = config('USE_TZ', default=True, cast=bool)
# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/var/www/example.com/static/"
STATIC_ROOT = os.path.join(BASE_DIR,  'static/')


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = config('STATIC_URL', default='/static/')

MEDIA_ROOT = config('MEDIA_ROOT', default=BASE_DIR + '/media/')

MEDIA_URL = config('MEDIA_URL', default='/media/')

TEMPLATE_DIRS = (os.path.join(BASE_DIR,  'templates'),)

PROJECT_NAME = 'DoCash'

LOGOUT_REDIRECT_URL = 'home'

LOGIN_URL = 'ikwen:sign_in'

LOGIN_REDIRECT_URL = 'home'

LOCAL_DEV = config('LOCAL_DEV', default=False, cast=bool)

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]


#  *******       E-mail CONFIGURATION       *******  #
EMAIL_HOST = config('EMAIL_HOST', default='localhost')
EMAIL_HOST_USER = config('EMAIL_HOST_USER', default='')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD', default='')
EMAIL_PORT = config('EMAIL_PORT', default=25, cast=int)
EMAIL_USE_TLS = config('EMAIL_USE_TLS', default=False, cast=bool)

